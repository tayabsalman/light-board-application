const LOCKSTATE = {
    OPEN:0,
    ACQUIRED:1,
    LOCKED:2
}

const LIGHTSTATE = {
    OFF:0,
    ON:1
}