function Framework()
{
}
Framework.controler_URL = "controller_ajax.php"
Framework.Ajax = function(mod, activity,data = {},callback = false)
{
    data.mod = mod
    data.activity = activity;
    
    $.ajax({
		type: "POST",
		data:data,
		complete: function( event){
			var reply = event.responseJSON;
           if(typeof(reply) == "undefined")
           {
               alert(event);
           }
           else
           {
                if(callback)
                {
                    callback(reply);
                }
           }
        }.bind(data),
        error: function(event){
            alert(event.responseJSON);
        },
        url: Framework.controler_URL,
        timeout: 10000
	});
};