$(document).ready(function(){
    Main.UpdateAllStates();
});
function Main(){
    this.lockState = LOCKSTATE.OPEN;
    this.lockNumber = 0;
}

Main.BindEvents = function()
{
    this.BindLockEvent();
    this.BindLightEvents();
}
Main.BindLockEvent = function()
{
    $("#acquire-lock").unbind().bind("click", function(e){
        Main.AcquireLock();
    }.bind(this));
}
Main.BindLightEvents = function()
{
    $(".js-light-clickable").unbind().bind("click", this, function(e){
        if(e.data.lockState != LOCKSTATE.ACQUIRED)
            return alert("Acquire Lock First.!");
        var data = {};
        data.lightId = $(this).attr("data-lightId");
        Framework.Ajax("controller", "changelightstate", data, function(response){
            if(response.result.state != "ok")
                return alert(response.result.message);
            this.HandleLightBoard(response.data);
        }.bind(e.data));
    });
}
Main.AcquireLock = function()
{
    Framework.Ajax("controller", "acquirelock", {}, function(response){
        if(response.result.state != "ok")
        {
            this.lockState = LOCKSTATE.OPEN;
            return alert(response.result.message);
        }
        this.HandleLockState(response.data);
    }.bind(this));
}
Main.HandleLightBoard = function(data)
{
    this.HandleLockState(data.lockState);
    this.HandleLightsState(data.content);
}
Main.HandleLockState = function(lockState)
{
    $("#acquire-lock").unbind();
    this.lockState = lockState;
    if(lockState == LOCKSTATE.OPEN)
    {
        $("#acquire-lock").html("Lock Available");
        $("#acquire-lock").removeClass("btn-warning btn-danger").addClass("btn-success");
        this.BindEvents();
        return ;
    }
    if(lockState == LOCKSTATE.ACQUIRED)
    {
        $("#acquire-lock").html("Lock Acquired");
        $("#acquire-lock").removeClass("btn-success btn-danger").addClass("btn-warning");
        return ;
    }
    $("#acquire-lock").html("Lock un-available");
    $("#acquire-lock").removeClass("btn-success btn-warning").addClass("btn-danger");
}
Main.HandleLightsState = function(board)
{
    $(".js-lightboard").html(board);
    this.BindLightEvents();
}
Main.UpdateAllStates = function()
{
    Framework.Ajax("controller", "refreshlightboard", {}, function(response){
        if(response.result.state != "ok")
            return console.log(response.result);
        this.HandleLightBoard(response.data);
        setTimeout(function(){
            this.UpdateAllStates();
        }.bind(this), 1000)
    }.bind(this));
}