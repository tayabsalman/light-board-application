<?php
if(!isset($_REQUEST['mod']) || !isset($_REQUEST['activity']))
	die("Internal Error");

//Setup Include Path
$whatEverLocation = implode("/", array_slice(explode("/", getcwd()), 0, -2));
$privatePath = $whatEverLocation ."/System/pvt";
set_include_path(get_include_path(). PATH_SEPARATOR . $privatePath);

require_once("system/exceptions.php");
require_once("system/authentication.php");

$result = array(
    "data"=> NULL,
    "result"=> array(
        "state"=>"",
        "meta"=>""
    )
);
try
{
    Authentication::Start();
    Authentication::Validate();
	$module = $_REQUEST['mod'];
	$activity = $_REQUEST['activity'];
	require_once("controller/$module.php");
	$controllerClass = $module."Controller";
	$controllerActivity = $activity."Ack";
	
	$controller = new $controllerClass();
	$data = $controller->$controllerActivity();
	
	$result["data"] = $data;
	$result["result"]["state"] ="ok";
}
catch(DBException $e)
{
    $result["result"]["state"] = "database Error";
    $result["result"]["message"] = $e->getMessage();
}
catch(InvalidDataException $e)
{
    $result["result"]["state"] = "invalid Data";
    $result["result"]["message"] = $e->getMessage();
}
catch(AuthException $e)
{
    $result["result"]["state"] = "un-authorized";
    $result["result"]["message"] = $e->getMessage();
}
catch(Exception $e)
{
    $result["result"]["state"] = "error";
    $result["result"]["message"] = $e->getMessage();
}
header("Content-Type: application/json");
echo json_encode($result);
$result = NULL;
?>