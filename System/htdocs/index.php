<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

//Setup include path
$whatEverLocation = implode("/", array_slice(explode("/", getcwd()), 0, -2));
$privatePath = $whatEverLocation ."/System/pvt";
set_include_path(get_include_path(). PATH_SEPARATOR . $privatePath);

require_once("system/authentication.php");
require_once("system/exceptions.php");
require_once("entity/entity.php");
require_once("conf/enums.php");

Authentication::Start();
$entityObj = new Entity();
if(!Authentication::IsAuthenticated())
{
    $showForm = true;
    if(isset($_POST["submit"]))
    {
        $email = isset($_POST['email']) && filter_var(trim($_POST["email"]), FILTER_VALIDATE_EMAIL)?trim($_POST["email"]) : "";
        if(strlen($email) == 0)
            echo "</p>Give a valid Email ID</p>";
        else
        {
            $userId = $entityObj->GetUserId($email);
            if($userId == -1)
                $userId = $entityObj->RegisterNewUser($email);
            $entityObj->userId = $userId;
            $userData = $entityObj->GetUserData();
            Authentication::Authenticate($userData);
            $showForm = false;
        }
    }
    if($showForm)
    {
        ?>
        <form method="post" action="/">
            <input type="text" name="email"><br>
            <input type="submit" name="submit" value="Login/Register"><br>
        </form>
        <?php
    }
}
if(Authentication::IsAuthenticated())
{
    $userData = Authentication::GetAuthData();
    $entityObj->userId = $userData["id"];
    $lightBoard = $entityObj->GetUserSpecificBoard();
    ?>
    <div class="container">
        <label class="font-weight-bold">Your Info:</label>
        <div class="row">
            <div class='col-md-6 col-sm-12'>
            <div class="form-group">
                <label class="font-weight-bold">Email:</label>
                <label><?= $userData["email"] ?></label>
            </div>
            </div>
            <div class='col-md-6 col-sm-12'>
            <div class="form-group">
                <label class="font-weight-bold">Color:</label>
                <span class="user-color" style="background-color:<?= $userData["color"] ?>;"></span>
            </div>
            </div>
        </div>
        <div class="lightboard js-lightboard"><?= $lightBoard ?></div>
        <div class="lock-wrapper">
            <button id="acquire-lock" type="button" class="btn btn-success btn-block">Lock Available</button>
        </div>
    </div>
    <link rel="stylesheet" type="text/css" href="vendors/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <script src="vendors/jquery/jquery.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/infra.js"></script>
    <script src="js/const.js"></script>
    <script src="js/main.js"></script>
    <script>var userId=<?=$userData["id"] ?></script>
    <?php
}
$entityObj->Close();
?>