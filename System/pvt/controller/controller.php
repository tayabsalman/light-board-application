<?php
require_once("controller/excontroller.php");
require_once("system/authentication.php");
require_once("entity/entity.php");
require_once("conf/enums.php");

class controllerController extends EXController
{
    public function acquirelockAck()
    {
        $userData = Authentication::GetAuthData();
        $entityObj = new Entity();
        $entityObj->userId = $userData["id"];
        $lockAcquired = $entityObj->AcquireLock();
        $entityObj->Close();
        if($lockAcquired)
            return LockState::Acquired;
        else
            return LockState::Locked;
    }

    public function changelightstateAck()
    {
        $lightId = $this->GetRequiredParam("lightId");
        $entityObj = new Entity();
        $entityObj->lightId = $lightId;
        $entityObj->userId = Authentication::GetAuthData()["id"];
        $entityObj->color = Authentication::GetAuthData()["color"];

        if(!$entityObj->UserHasAcquiredLock())
        {
            $entityObj->Close();
            throw new InvalidDataException("You haven't acquired lock, acquire it first");
        }
        if(!$entityObj->CanUserChangeLight())
        {
            $entityObj->Close();
            throw new InvalidDataException("You cannot change other user's light");
        }

        $entityObj->ChangeLightStatus();
        $data = array();
        $data["lockState"] = $entityObj->GetUserSpecificLockState();
        $data["content"] = $entityObj->GetUserSpecificBoard();
        $entityObj->Close();
        return $data;
    }

    public function refreshlightboardAck()
    {
        $data = array();
        $userData = Authentication::GetAuthData();
        $entityObj = new Entity();
        $entityObj->userId = $userData["id"];
        
        $entityObj->RemoveExpiredLock();
        $data["lockState"] = $entityObj->GetUserSpecificLockState();
        $data["content"] = $entityObj->GetUserSpecificBoard();
        $entityObj->Close();
        return $data;
    }
}