-- Populating Colors data -- 
USE LightApplication;
INSERT INTO `Colors`(`code`)
VALUES ("#000000"), ("#00008B"), ("#0000FF"), ("#003300"), ("#00688B"), ("#00C5CD"), ("#00CD00"), ("#0147FA"), ("#2E0854"), ("#2E6444"), ("#54632C"), ("#551011"), ("#551033"), ("#CD0000"), ("#CD69C9"), ("#CDAD00"), ("#FFB533"), ("#F6FF33"), ("#33F9FF"), ("#FF33EC");