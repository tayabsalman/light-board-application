<?php
class Authentication
{
    public static function Start()
    {
        if(session_status() !== PHP_SESSION_ACTIVE)
            session_start();
    }
    public static function Validate()
    {
        if(self::IsAuthenticated())
            return;
        throw new AuthException("Access Denied");
    }
    public static function Authenticate($data = array())
    {
        self::Start();
        $_SESSION["data"] = $data;
        $_SESSION["authenticated"] = true;
    }
    public static function IsAuthenticated()
    {
        if(session_status() == PHP_SESSION_ACTIVE && isset($_SESSION["authenticated"]) && $_SESSION["authenticated"] == true)
            return true;
        return false;
    }
    public static function GetAuthData()
    {
        if(!self::IsAuthenticated())
            throw new AuthException("Un-Authorized user");
        return $_SESSION["data"];
    }
}