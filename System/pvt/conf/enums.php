<?php
//This file and const.js file of frontend will be mapped
abstract class BoardSize
{
    const TotalRows = 10;
    const TotalColumns = 10;
}
abstract class LightColors
{
    const Default = "#FFFFFF";
}
abstract class LightState
{
    const Off = 0;
    const On = 1;
}
abstract class LockState
{
    const Open = 0; //no one acquired the lock
    const Acquired = 1; //specific user has equired the lock
    const Locked = 2; //some other user has acquired the lock
}
abstract class LockSettings
{
    const Expiry = 120; //In seconds
}
?>