<?php
require_once("conf/config.php");
//All DB related stuff is here
class ExEntity
{
    private $host;
    private $user;
    private $password;
    private $dbName;
    private $con;
    private $result;

    public function __construct()
    {
        //Reading MySQL connection details from config
        global $CONFIG;
        $mysqlConfig = $CONFIG["mysql"];

        $this->host = $mysqlConfig["host"];
        $this->user = $mysqlConfig["user"];
        $this->password = $mysqlConfig["password"];
        $this->dbName = $mysqlConfig["database"];
        $this->Init();
    }
    public function __destruct()
    {
        $this->Close();
    }
    public function Init()
    {
        if($this->con == NULL)
        {
            $this->con = mysqli_connect(
                $this->host,
                $this->user,
                $this->password,
                $this->dbName
            );
        }
    }
    public function Close()
    {
        if($this->con != NULL)
        {
            mysqli_close($this->con);
            $this->con = NULL;
        }
    }
    public function Query($query)
    {
        if($this->con == NULL)
            $this->Init();
        $this->result = mysqli_query($this->con, $query);

        if(!$this->result)
        {
            throw new DBException(mysqli_error($this->con));
        }
    }
    public function GetItem()
    {
        return mysqli_fetch_array($this->result, MYSQLI_ASSOC);
    }
    public function GetAllItems()
    {
        return mysqli_fetch_all($this->result, MYSQLI_ASSOC);
    }
    public function GetGeneratedId()
    {
        return mysqli_insert_id($this->con);
    }
    public function GetQueryResult()
    {
        return $this->result;
    }
    public function Escape($str)
    {
        return mysqli_real_escape_string($this->con, $str);
    }
}
?>