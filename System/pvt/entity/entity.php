<?php
require_once("entity/exentity.php");
require_once("conf/enums.php");

class entity extends EXEntity
{
    public function __construct()
    {
        parent::__construct();
    }
    public function GetUserId($email)
    {
        $userId = -1;
        $query = "SELECT `id` FROM `Users` WHERE `email` = '". $this->Escape($email) . "'";
        $this->Query($query);
        if($row = $this->GetItem())
            $userId = $row["id"];
        return $userId;
    }
    public function RegisterNewUser($email)
    {
        $userId = -1;
        $colorCode = $this->GetColorForNewUser();
        $query = "INSERT INTO `Users`(`email`, `color`) ".
                 "VALUES('" . $this->Escape($email) . "', '" . $this->Escape($colorCode) . "')";
        $this->Query($query);
        $userId = $this->GetGeneratedId();

        if($userId == -1)
        {
            $this->Close();
            throw new InvalidDataException("We are under maintenace, Will be back soon");
        }
        return $userId;
    }
    public function GetColorForNewUser()
    {
        $colorId = 0;
        $colorCode = "";
        $query = "SELECT `id` FROM `Colors` WHERE `lastUsed`='1'";
        $this->Query($query);
        if($row = $this->GetItem())
        {
            $query = "UPDATE `Colors` SET `lastUsed`='0' WHERE `id`='" . $row['id'] . "'";
            $this->Query($query);
            if($this->GetQueryResult())
            {
                $query = "SELECT `id`, `code` FROM `Colors` WHERE `id` > '" . $row['id'] . "' LIMIT 1";
                $this->Query($query);
                if($row = $this->GetItem())
                {
                    $colorId = $row["id"];
                    $colorCode = $row["code"];
                }
            }
        }

        if($colorId == 0)
        {
            $query = "SELECT `id`, `code` FROM `Colors` ORDER BY `id` ASC LIMIT 1";
            $this->Query($query);
            if($row = $this->GetItem())
            {
                $colorId = $row["id"];
                $colorCode = $row["code"];
            }
        }

        $query = "UPDATE `Colors` SET `lastUsed`='1' WHERE `id` = $colorId";
        $this->Query($query);
        return $colorCode;
    }
    public function GetUserData()
    {
        $userData = array();
        $query = "SELECT * FROM `Users` WHERE `id` = '". $this->Escape($this->userId) . "'";
        $this->Query($query);
        if($row = $this->GetItem())
        {
            $userData["id"] = $row["id"];
            $userData["email"] = $row["email"];
            $userData["color"] = $row["color"];
        }
        return $userData;
    }
    public function GetLightsStatus()
    {

        $query = "SELECT * FROM `LightStatus` ORDER BY `lightId` ASC";
        $this->Query($query);
        $lightsStatus = array();
        while($row = $this->GetItem())
            $lightsStatus[$row["lightId"]] = $row;
        return $lightsStatus;

    }
    public function GetUserSpecificBoard()
    {
        $lightsStatus = $this->GetLightsStatus();
        $lightId = 1;
        $lightBoardHTML = "";
        for($rowNo = 0; $rowNo < BoardSize::TotalRows ; $rowNo++)
        {
            for($columnNo = 0; $columnNo < BoardSize::TotalColumns; $columnNo++)
            {
                $lightColor = LightColors::Default;
                $className = "js-light-clickable";
                if(isset($lightsStatus[$lightId]))
                {
                    $lightColor = ($lightsStatus[$lightId]["state"] == LightState::On)? $lightsStatus[$lightId]["color"] : LightColors::Default;
                    if($lightsStatus[$lightId]["userId"] != $this->userId && $lightsStatus[$lightId]["state"] == LightState::On)
                        $className = "js-light-unclickable";
                }
                $lightBoardHTML .= "<div data-lightId='$lightId' class='light $className' style='background-color:$lightColor;'></div>";
                $lightId++;
            }
        }
        return $lightBoardHTML;
    }

    public function AcquireLock()
    {
        $query = "SELECT `state` FROM `LockStatus` WHERE `userId` != '".$this->Escape($this->userId) ."' ".
                 "AND `state` = '".LockState::Acquired ."'";
        $this->Query($query);
        if($row = $this->GetItem())
            return false;
        
        $query = "INSERT INTO `LockStatus`(`state`, `userId`) " .
                 "VALUES('" . $this->Escape(LockState::Acquired)."', '" . $this->escape($this->userId) ."')";
        $this->Query($query);
        if($this->GetQueryResult())
            return true;
        return false;
    }

    public function RemoveLockUnconditionally()
    {
        $query = "DELETE FROM `LockStatus`";
        $this->Query($query);
    }

    public function UserHasAcquiredLock()
    {
        $query = "SELECT `state` FROM `LockStatus` WHERE `userId`='". $this->Escape($this->userId) . "'";
        $this->Query($query);
        if($row = $this->GetItem())
            if($row["state"] == LockState::Acquired)
                return true;
        return false;
    }

    public function CanUserChangeLight()
    {
        $query = "SELECT * FROM `LightStatus` WHERE `lightId`='". $this->Escape($this->lightId) . "'";
        $this->Query($query);
        $row = $this->GetItem();
        if($row && $row["userId"] != $this->userId && $row["state"] == LightState::On)
            return false;
        return true;
    }

    public function ChangeLightStatus()
    {
        $query = "INSERT INTO `LightStatus`(`lightId`, `userId`, `state`, `color`) " .
                 "VALUES('" . $this->Escape($this->lightId)."', '" . $this->escape($this->userId) ."'". 
                 ", '" . $this->Escape(LightState::On). "', '" . $this->Escape($this->color) ."') " .
                 "ON DUPLICATE KEY UPDATE `userId`='" . $this->Escape($this->userId). "', `state` = ABS(`state` - 1) " . 
                 ", `color` = '" . $this->Escape($this->color) . "'";
        $this->Query($query);
        $this->RemoveLockUnconditionally();
    }

    public function RemoveExpiredLock()
    {
        $query = "SELECT `lockedOn` FROM `LockStatus` WHERE `lockedOn` < DATE_SUB(NOW(), INTERVAL " . LockSettings::Expiry . " SECOND)";
        $this->Query($query);
        if($row = $this->GetItem())
            $this->RemoveLockUnconditionally();
    }

    public function GetUserSpecificLockState()
    {
        $query = "SELECT `state`, `userId` FROM `LockStatus`";
        $this->Query($query);
        if($row = $this->GetItem())
        {
            if($row["userId"] != $this->userId)
                return LockState::Locked;
            return LockState::Acquired;
        }
        return LockState::Open;
    }
}