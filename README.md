# Simple Light Application
A simple light board application shared among multiple users.
A user can change the state of the light after successfully acquiring the lock on the light board.

## Dependencies/Installations
```
PHP version 7.2
MySQL version 5.7
Frontend library: JQuery and bootstrap
Note: This project can be placed in any directory and it will work as expected.
PHP 7.2 comes with module to connect to mysql, if you are unable to connect install in manually by the following command
$ sudo apt install php7.2-mysql
```
## Before starting
```
Before performing the below mentioned steps make sure you have installed the dependencies
1. Take the SQL written in /System/pvt/system/DB.sql and execute in MySQL server
2. Take insert  queries from /System/pvt/system/initailDataLoad.sql and execute in MySQL server
3. Update Mysql connection details such as host, user, password and database in /System/pvt/conf/config.php in order to connect to MySQL server.
```

##Starting the Application
```
Before starting the application, install the dependencies and perform the steps mentioned in Before Starting block
1.Place the System Directory anywhere in your filesystem
2.Configure the webserver to access this System folder, and set index to index.php present inside /System/htdocs directory
3.Visit setup domain and application will start running.
```

## How to use the Application?
```
1.Register with a valid Email ID
2.Acquire lock before turning the light ON/OFF
3.After acquiring lock successfully, turn the light ON/OFF
```
### NOTE:
```
1.Lock cannot be revoke by the user without changing the light state
2.Lock will automatically gets expoired after predfined time.(for example: 120 seconds)
3.One user cannot OFF the light turned ON by some other user
```

## Built with
* Backend Language: PHP(7.2)
* Database : MySQL(5.7)
* Frontend : JQuery(3.4), JavaScript
* Styling : Bootstrap along with  CSS
* Sync : AJAX long polling.
